<?php
return [
    'default' => 'redis',
    'stores' => [
        'file' => [
            'type' => 'File',
            // 缓存保存目录
            'path' => runtime_path() . '/cache/',
        ],
        'redis' => [
            'type' => 'redis',
            'host' => '127.0.0.1',
            'password' => '',
            'port' => 6379,
            'select' => 0,
        ],
    ],
];